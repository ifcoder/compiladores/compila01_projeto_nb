
import parser.Parser;
import java.io.FileReader;
import scanner.Scanner;

public class App {

    public static void main(String[] args) throws Exception {
        try {
            FileReader file = new FileReader("teste.txt");
            Scanner scanner = new Scanner(file);

           //Scanner scanner = new Scanner(System.in);
            Parser p = new Parser(scanner);
            Object obj = p.parse().value;
            if(obj == null)
                System.out.println("Arquivo sem erro de sintaxe!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
